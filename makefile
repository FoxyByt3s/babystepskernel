boot.img: boot.bin
	dd if=boot.bin of=boot.img bs=512 count=1 conv=notrunc #from sector 0 and 1 sector length
boot.bin: boot.asm
	nasm boot.asm -f bin -o boot.bin